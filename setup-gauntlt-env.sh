#!/bin/bash

#Download arachni binary
wget https://github.com/Arachni/arachni/releases/download/v1.5.1/arachni-1.5.1-0.5.12-linux-x86_64.tar.gz
tar -xvf arachni-1.5.1-0.5.12-linux-x86_64.tar.gz

#cd into src directory to start install scripts
cd gauntlt-fork

#Update rake (ruby package manager)
gem install rake

#install rvm
gem install rvm

#Installing gauntlt with ruby package manager
gem install gauntlt
