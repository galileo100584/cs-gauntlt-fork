## Continuous Security med gauntlt 
Dette repo kan klones for å få bruke et Docker image til å kjøre [gauntlt](http://gauntlt.org/) i
Jenkins Pipeline via Harbor eller som standalone/ sandbox. 

Tester må skreves med Gherkin syntax: Cucumber, og ligger her:
gauntlt-fork/examples


#### Bygg docker image på host:
```
docker build --tag=debian-gauntlt .
```
##### Kjør docker container fra host:
```
docker run -t --name auto-pen-test<number> debian-gauntlt
```
##### Kopier resultat fra container (tilbake til host):
```
docker cp auto-pen-test<number>:/root/work/gauntlt-fork/result<test-name>.html /root/result<test-name>.html
```
##### Evt. kopier fra root-bruker til lokal-bruker sin katalog:
```
cp /root/result<test-name>.html /home/<user-name>
```
